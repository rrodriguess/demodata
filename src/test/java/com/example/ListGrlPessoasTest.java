package com.example;

import com.example.domain.GrlPessoa;
import com.example.repository.GrlPessoaDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ListGrlPessoasTest {

    @Autowired
    private GrlPessoaDao grlPessoaDao;

    @Test
    public void testGetList() {
        List<GrlPessoa> grlPessoa = grlPessoaDao.getListGrlPessoas();

        System.out.println(grlPessoa.size());

        assertEquals(!grlPessoa.isEmpty(), !grlPessoa.isEmpty());
    }


}
