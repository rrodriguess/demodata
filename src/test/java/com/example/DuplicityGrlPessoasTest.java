package com.example;

import com.example.domain.GrlPessoa;
import com.example.repository.GrlPessoaDao;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DuplicityGrlPessoasTest {

    @Autowired
    private GrlPessoaDao grlPessoaDao;

    public void testDuplicates() {
        List<GrlPessoa> grlPessoas = grlPessoaDao.findByGrlPessoaFisica_Cpf(new Long(22703395841l));
    }
}
