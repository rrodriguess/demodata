package com.example;

import com.example.domain.GrlCidade;
import com.example.repository.GrlCidadeDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoDataApplicationTests {

	@Autowired
	public GrlCidadeDao grlCidadeDao;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testLoadingCidade() {
		GrlCidade cidade = grlCidadeDao.findByNomeCidadeOrIbgeMun("SAO PAULO", new Long(3550308));

		Assert.assertEquals("SAO PAULO", cidade.getNomeCidade());
	}

}
