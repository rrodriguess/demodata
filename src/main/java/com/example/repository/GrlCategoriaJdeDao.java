package com.example.repository;

import com.example.domain.GrlCategoriaJde;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface GrlCategoriaJdeDao extends PagingAndSortingRepository<GrlCategoriaJde, String> {

}
