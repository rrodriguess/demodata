package com.example.repository;

import com.example.domain.GrlCategoria;
import com.example.domain.GrlCategoriaJde;
import com.example.domain.GrlCategoriaVsJde;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface GrlCategoriaVsJdeDao extends PagingAndSortingRepository<GrlCategoriaVsJde, Long> {

    GrlCategoriaVsJde readByGrlCategoriaAndGrlCategoriaJde(GrlCategoria grlCategoria, GrlCategoriaJde grlCategoriaJde);

    @Query(value = "select * from CAP.GRL_CATEGORIA_VS_JDE where ID_CATEGORIA = ?1 and ID_CATEG_JDE = ?2", nativeQuery = true)
    GrlCategoriaVsJde findByGrlCategoriaAndGrlCategoriaJde(Long idCategoria, String idCategoriaJde);

}