package com.example.repository;

import com.example.domain.GrlPessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface GrlPessoaDao extends JpaRepository<GrlPessoa, Long> {

    @Query(value = "SELECT P FROM GRL_PESSOA P, GRL_PESSOA_FISICA PF, GRL_CATEGORIA_VS_JDE CategJde \n" +
                    "WHERE PF.cpf = ?1 " +
                    "AND P.idPessoa = PF.idPessoa " +
                    "AND P.grlCategoria = CategJde.grlCategoria " +
                    "AND \n" +
                    "P.grlCategoriaJde = CategJde.grlCategoriaJde \n" +
                    "ORDER BY ORDEM DESC, P.exportado ASC, P.alterado ASC")
    List<GrlPessoa> findByGrlPessoaFisica_Cpf(Long cpf);

    @Query(value = "SELECT P FROM GRL_PESSOA P, SCR_ASSOCIADO ASSOC, GRL_CATEGORIA_VS_JDE CategJde \n" +
                    "WHERE ASSOC.cpf = ?1 " +
                    "AND P.idPessoa = ASSOC.grlPessoa " +
                    "AND P.grlCategoria = CategJde.grlCategoria " +
                    "AND \n" +
                    "P.grlCategoriaJde = CategJde.grlCategoriaJde \n" +
                    "ORDER BY ORDEM DESC, P.exportado ASC, P.alterado ASC")
    List<GrlPessoa> findByScrAssociado_Cpf(Long cpf);

    @Query(value = "SELECT P FROM GRL_PESSOA P, GRL_PESSOA_JURIDICA PJ, GRL_CATEGORIA_VS_JDE CategJde \n" +
                    "WHERE PJ.cnpj = ?1 " +
                    "AND P.idPessoa = PJ.idPessoa " +
                    "AND P.grlCategoria = CategJde.grlCategoria " +
                    "AND \n" +
                    "P.grlCategoriaJde = CategJde.grlCategoriaJde \n" +
                    "ORDER BY ORDEM DESC, P.exportado ASC, P.alterado ASC")
    List<GrlPessoa> findByGrlPessoaJuridica_Cnpj(Long cnpj);

    @Query(value = "SELECT * " +
                    "FROM CAP.GRL_PESSOA PE " +
                    "WHERE PE.ID_PESSOA_LINK IS NULL " +
                    "AND PE.EXPORTADO IS NOT NULL " +
                    "AND PE.EXPORTADO > SYSDATE - 100 " +
                    "AND PE.EXPORTADO < SYSDATE + 1 " +
                    "AND PE.ID_CATEGORIA <> 3", nativeQuery = true)
    List<GrlPessoa> getListGrlPessoas();
}