package com.example.repository;

import com.example.domain.GrlCidade;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface GrlCidadeDao extends PagingAndSortingRepository<GrlCidade, Long> {

    GrlCidade findByNomeCidadeOrIbgeMun(String nomeCidade, Long ibgeMun);

}