package com.example.repository;

import com.example.domain.GrlPessoaJuridica;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface GrlPessoaJuridicaDao extends CrudRepository<GrlPessoaJuridica, Long> {
	
	List<GrlPessoaJuridica> findByCnpj(Long cnpj);
	
}
