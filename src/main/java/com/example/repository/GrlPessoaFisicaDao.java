package com.example.repository;

import com.example.domain.GrlPessoaFisica;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface GrlPessoaFisicaDao extends PagingAndSortingRepository<GrlPessoaFisica, Long> {

}
