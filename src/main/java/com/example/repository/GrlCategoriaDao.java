package com.example.repository;

import com.example.domain.GrlCategoria;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface GrlCategoriaDao extends PagingAndSortingRepository<GrlCategoria, Long> {

    GrlCategoria findByIdCategJdePadrao(String tpPessoa);
}
