package com.example.repository;

import com.example.domain.ScrAssociado;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface ScrAssociadoDao extends PagingAndSortingRepository<ScrAssociado, Long> {

}
