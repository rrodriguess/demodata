package com.example;

import com.example.domain.GrlPessoa;
import com.example.domain.IdLink;
import com.example.repository.GrlPessoaDao;
import com.example.util.WriterGrlPessoas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoDataApplication implements CommandLineRunner{

	@Autowired
	private GrlPessoaDao grlPessoaDao;

	public static void main(String[] args) {
		SpringApplication.run(DemoDataApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		List<GrlPessoa> grlPessoas = grlPessoaDao.getListGrlPessoas();
		File file = new File("pessoas");

		if (!file.exists()) {
			file.mkdir();
		}

		file.delete();

		WriterGrlPessoas writer = new WriterGrlPessoas();
		writer.action("antes", grlPessoas);

		for (GrlPessoa grlPessoa : grlPessoas) {
			IdLink idLink = new IdLink(grlPessoaDao);
			if (grlPessoa.getExportado() != null) {
				idLink.insert(grlPessoa);
			}
			System.out.println("Id: " + grlPessoa.getIdPessoa() + ", Nome: " +grlPessoa.getNome() + ", IdLink: " + grlPessoa.getIdPessoaLink());
		}

		List<GrlPessoa> grlPessoasUpdates = new ArrayList<>();
		for(GrlPessoa pessoa : grlPessoas) {
			GrlPessoa newGrlPessoa = grlPessoaDao.findOne(pessoa.getIdPessoa());
			grlPessoasUpdates.add(newGrlPessoa);
		}

		writer.action("depois", grlPessoasUpdates);
	}
}
