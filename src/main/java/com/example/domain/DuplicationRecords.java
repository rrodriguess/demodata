package com.example.domain;

import com.example.repository.GrlPessoaDao;
import com.example.util.Document;
import com.example.util.DocumentAssociado;
import com.example.util.DocumentPessoaFisica;
import com.example.util.DocumentPessoaJuridica;
import javafx.scene.web.WebEngine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 09/09/16.
 */
public class DuplicationRecords {

    private final GrlPessoaDao grlPessoaDao;
    private final GrlPessoa grlPessoa;

    public DuplicationRecords(GrlPessoa pGrlPessoa, GrlPessoaDao grlPessoaDao) {
        this.grlPessoaDao = grlPessoaDao;
        this.grlPessoa = pGrlPessoa;
    }

    public List<GrlPessoa> getListOfDuplicates() {
        Long document = getDocument();
        List<GrlPessoa> grlPessoas = new ArrayList<>();

            if (grlPessoa.getPessoaTp() == 0) {
                List<GrlPessoa> grlAssociados = grlPessoaDao.findByScrAssociado_Cpf(document);
                List<GrlPessoa> grlPessoasFisicas = grlPessoaDao.findByGrlPessoaFisica_Cpf(document);
                grlPessoas.addAll(grlAssociados);
                grlPessoas.addAll(grlPessoasFisicas);
            } else {
                List<GrlPessoa> grlPessoasJuridicas = grlPessoaDao.findByGrlPessoaJuridica_Cnpj(document);
                grlPessoas.addAll(grlPessoasJuridicas);
            }

        return grlPessoas;
    }

    private Long getDocument() {
        DocumentPessoaFisica docPf = new DocumentPessoaFisica();
        DocumentAssociado docAss = new DocumentAssociado();
        DocumentPessoaJuridica docPj = new DocumentPessoaJuridica();
        Long document = null;

        if (grlPessoa.getPessoaTp() == 0) {
            document = docPf.getDocument(grlPessoa);
            if (document == null)
                document = docAss.getDocument(grlPessoa);
        } else
            document = docPj.getDocument(grlPessoa);

        return document;
    }




}
