package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity(name = "GRL_CATEGORIA")
@Table(schema = "CAP")
public class GrlCategoria implements Serializable {
    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_CATEGORIA", unique = true, nullable = false)
    private Long idCategoria;

    @Column(name = "CABECA_DA_SUBCONTA")
    private String cabecaDaSubconta;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "INSCRICAO_BIB_VENCE")
    private Long inscricaoBibVence;

    @Column(name = "PODE_MANUTENCAO")
    private Long podeManutencao;

    @Column(length = 40)
    private String tabela;

    @Column(name = "USADO_EM_OUTRA_COBRANCA")
    private Long usadoEmOutraCobranca;

    @Column(name = "USADO_EM_OUTRO_PAGAMENTO")
    private Long usadoEmOutroPagamento;

    @Column(name = "ID_CATEG_JDE_PADRAO")
    private String idCategJdePadrao;

    public GrlCategoria() {
    }

    public Long getIdCategoria() {
        return this.idCategoria;
    }

    public void setIdCategoria(Long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCabecaDaSubconta() {
        return this.cabecaDaSubconta;
    }

    public void setCabecaDaSubconta(String cabecaDaSubconta) {
        this.cabecaDaSubconta = cabecaDaSubconta;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getInscricaoBibVence() {
        return this.inscricaoBibVence;
    }

    public void setInscricaoBibVence(Long inscricaoBibVence) {
        this.inscricaoBibVence = inscricaoBibVence;
    }

    public Long getPodeManutencao() {
        return this.podeManutencao;
    }

    public void setPodeManutencao(Long podeManutencao) {
        this.podeManutencao = podeManutencao;
    }

    public String getTabela() {
        return this.tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    public Long getUsadoEmOutraCobranca() {
        return this.usadoEmOutraCobranca;
    }

    public void setUsadoEmOutraCobranca(Long usadoEmOutraCobranca) {
        this.usadoEmOutraCobranca = usadoEmOutraCobranca;
    }

    public Long getUsadoEmOutroPagamento() {
        return this.usadoEmOutroPagamento;
    }

    public void setUsadoEmOutroPagamento(Long usadoEmOutroPagamento) {
        this.usadoEmOutroPagamento = usadoEmOutroPagamento;
    }

    public String getIdCategJdePadrao() {
        return idCategJdePadrao;
    }

    public void setIdCategJdePadrao(String idCategJdePadrao) {
        this.idCategJdePadrao = idCategJdePadrao;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GrlCategoria{");
        sb.append("idCategoria=").append(idCategoria);
        sb.append(", cabecaDaSubconta='").append(cabecaDaSubconta).append('\'');
        sb.append(", descricao='").append(descricao).append('\'');
        sb.append(", inscricaoBibVence=").append(inscricaoBibVence);
        sb.append(", podeManutencao=").append(podeManutencao);
        sb.append(", tabela='").append(tabela).append('\'');
        sb.append(", usadoEmOutraCobranca=").append(usadoEmOutraCobranca);
        sb.append(", usadoEmOutroPagamento=").append(usadoEmOutroPagamento);
        sb.append(", idCategJdePadrao='").append(idCategJdePadrao).append('\'');
        sb.append('}');
        return sb.toString();
    }
}