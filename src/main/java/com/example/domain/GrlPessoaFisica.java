package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name = "GRL_PESSOA_FISICA")
@Table(schema = "CAP")
public class GrlPessoaFisica implements Serializable {

    @Id
    @Column(name = "ID_PESSOA")
    private Long idPessoa;

    @Column(name = "CPF")
    private Long cpf;

    @Column(name = "RG")
    private String rg;

    @Column(name = "IDENTIDADE_EXTRANGEIRA")
    private String identidadeExtrangeira;

    @Column(name = "CCM")
    private Long ccm;

    @Column(name = "PIS")
    private Long pis;

    @Column(name = "PASEP")
    private Long pasep;

    @Column(name = "INSC_INSS")
    private Long InscInss;

    @Column(name = "ID_PROFISSAO")
    private Long idProfissao;

    @Column(name = "ID_CARGO")
    private Long idCargo;

    @Column(name = "ID_PESSOA_EMPRESA")
    private Long idPessoaEmpresa;

    @Column(name = "CRM")
    private String crm;

    @Column(name = "COD_PROD_AGRICOLA")
    private String codProdAgricola;

    @Column(name = "NASCIMENTO")
    private Date nascimento;

    @Column(name = "RECADASTRAMENTO")
    private Timestamp recadastramento;

    public Long getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getIdentidadeExtrangeira() {
        return identidadeExtrangeira;
    }

    public void setIdentidadeExtrangeira(String identidadeExtrangeira) {
        this.identidadeExtrangeira = identidadeExtrangeira;
    }

    public Long getCcm() {
        return ccm;
    }

    public void setCcm(Long ccm) {
        this.ccm = ccm;
    }

    public Long getPis() {
        return pis;
    }

    public void setPis(Long pis) {
        this.pis = pis;
    }

    public Long getPasep() {
        return pasep;
    }

    public void setPasep(Long pasep) {
        this.pasep = pasep;
    }

    public Long getInscInss() {
        return InscInss;
    }

    public void setInscInss(Long inscInss) {
        InscInss = inscInss;
    }

    public Long getIdProfissao() {
        return idProfissao;
    }

    public void setIdProfissao(Long idProfissao) {
        this.idProfissao = idProfissao;
    }

    public Long getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Long idCargo) {
        this.idCargo = idCargo;
    }

    public Long getIdPessoaEmpresa() {
        return idPessoaEmpresa;
    }

    public void setIdPessoaEmpresa(Long idPessoaEmpresa) {
        this.idPessoaEmpresa = idPessoaEmpresa;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getCodProdAgricola() {
        return codProdAgricola;
    }

    public void setCodProdAgricola(String codProdAgricola) {
        this.codProdAgricola = codProdAgricola;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public Timestamp getRecadastramento() {
        return recadastramento;
    }

    public void setRecadastramento(Timestamp recadastramento) {
        this.recadastramento = recadastramento;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GrlPessoaFisica{");
        sb.append("idPessoa=").append(idPessoa);
        sb.append(", cpf=").append(cpf);
        sb.append(", rg='").append(rg).append('\'');
        sb.append(", identidadeExtrangeira='").append(identidadeExtrangeira).append('\'');
        sb.append(", ccm=").append(ccm);
        sb.append(", pis=").append(pis);
        sb.append(", pasep=").append(pasep);
        sb.append(", InscInss=").append(InscInss);
        sb.append(", idProfissao=").append(idProfissao);
        sb.append(", idCargo=").append(idCargo);
        sb.append(", idPessoaEmpresa=").append(idPessoaEmpresa);
        sb.append(", crm='").append(crm).append('\'');
        sb.append(", codProdAgricola='").append(codProdAgricola).append('\'');
        sb.append(", nascimento=").append(nascimento);
        sb.append(", recadastramento=").append(recadastramento);
        sb.append('}');
        return sb.toString();
    }
}