package com.example.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "GRL_CATEGORIA_VS_JDE")
@Table(schema = "CAP")
public class GrlCategoriaVsJde implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private GrlCategoriaVsJdePK grlCategoriaVsJdePK;

    @ManyToOne
    @JoinColumn(name = "ID_CATEGORIA", insertable = false, updatable = false)
    private GrlCategoria grlCategoria;

    @ManyToOne
    @JoinColumn(name = "ID_CATEG_JDE", nullable = false, insertable = false, updatable = false)
    private GrlCategoriaJde grlCategoriaJde;

    @Column(name = "ORDEM")
    private Long ordem;

    public GrlCategoriaVsJde() {
    }

    public GrlCategoriaVsJdePK getGrlCategoriaVsJdePK() {
        return grlCategoriaVsJdePK;
    }

    public void setGrlCategoriaVsJdePK(GrlCategoriaVsJdePK grlCategoriaVsJdePK) {
        this.grlCategoriaVsJdePK = grlCategoriaVsJdePK;
    }

    public GrlCategoria getGrlCategoria() {
        return grlCategoria;
    }

    public void setGrlCategoria(GrlCategoria grlCategoria) {
        this.grlCategoria = grlCategoria;
    }

    public GrlCategoriaJde getGrlCategoriaJde() {
        return grlCategoriaJde;
    }

    public void setGrlCategoriaJde(GrlCategoriaJde grlCategoriaJde) {
        this.grlCategoriaJde = grlCategoriaJde;
    }

    public Long getOrdem() {
        return ordem;
    }

    public void setOrdem(Long ordem) {
        this.ordem = ordem;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GrlCategoriaVsJde{");
        sb.append("grlCategoriaVsJdePK=").append(grlCategoriaVsJdePK);
        sb.append(", grlCategoria=").append(grlCategoria);
        sb.append(", grlCategoriaJde=").append(grlCategoriaJde);
        sb.append(", ordem=").append(ordem);
        sb.append('}');
        return sb.toString();
    }
}