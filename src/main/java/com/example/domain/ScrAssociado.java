package com.example.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity(name = "SCR_ASSOCIADO")
@Table(schema = "CAP")
public class ScrAssociado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false, name = "ID")
    private Long id;

    @Column(name = "ADMISSAO")
    private Timestamp admissao;

    @Column(name = "ALTERACAO")
    private Timestamp alteracao;

    @Column(name = "BRASILEIRO")
    private BigDecimal brasileiro;

    @Column(name = "CADASTRO")
    private Timestamp cadastro;

    @Column(name = "CARTEIRA")
    private Timestamp carteira;

    @Column(name = "CELULAR")
    private String celular;

    @Column(name = "CLASSE")
    private Long classe;

    @Column(name = "COBRANCA")
    private Long cobranca;

    @Column(name = "CONDICAO")
    private Long condicao;

    @Column(name = "CONTRIBUICAO")
    private Long contribuicao;

    @Column(name = "CPF", columnDefinition = "VARCHAR2(15 BYTE)")
    private Long cpf;

    @Column(name = "DDDCELULAR")
    private String dddcelular;

    @Column(name = "DESATIVACAO")
    private Timestamp desativacao;

    @Column(name = "DIREITOSSOCIAIS")
    private Timestamp direitossociais;

    @Column(name = "EFETIVIDADE")
    private Timestamp efetividade;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ESTCIVIL")
    private Long estcivil;

    @Column(name = "EXAME")
    private Timestamp exame;

    @Column(name = "FOTO")
    private Timestamp foto;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PESSOA")
    private GrlPessoa grlPessoa;

    @Column(name = "INSTRUCAO")
    private Long instrucao;

    @Column(name = "MORTO")
    private Long morto;

    @Column(name = "NACIONALIDADE")
    private Long nacionalidade;

    @Column(name = "NASCIMENTO")
    private Timestamp nascimento;

    @Column(name = "NECESSIDADE_ESPECIAL")
    private Long necessidadeEspecial;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "NOMEMAE")
    private String nomemae;

    @Column(name = "NOMEPAI")
    private String nomepai;

    @Column(name = "PARENTESCO")
    private Long parentesco;

    @Column(name = "PROFISSAO")
    private Long profissao;

    @Column(name = "PROPONENTE1")
    private Long proponente1;

    @Column(name = "PROPONENTE2")
    private Long proponente2;

    @Column(name = "PROPOSTA")
    private Long proposta;

    @Column(name = "RECADASTRO")
    private Timestamp recadastro;

    @Column(name = "RG")
    private String rg;

    @Column(name = "SEXO")
    private BigDecimal sexo;

    @Column(name = "SITDTFIM")
    private Timestamp sitdtfim;

    @Column(name = "SITDTINI")
    private Timestamp sitdtini;

    @Column(name = "TITULO")
    private Long titulo;

    @Column(name = "TRATAMENTO")
    private Long tratamento;

    @Column(name = "ULTNOME")
    private String ultnome;

    @Column(name = "VETERANO")
    private Long veterano;

    @Column(name = "VIA")
    private BigDecimal via;

    @ManyToOne
    @JoinColumn(name = "RESPONSAVEL")
    private ScrAssociado scrAssociado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getAdmissao() {
        return admissao;
    }

    public void setAdmissao(Timestamp admissao) {
        this.admissao = admissao;
    }

    public Timestamp getAlteracao() {
        return alteracao;
    }

    public void setAlteracao(Timestamp alteracao) {
        this.alteracao = alteracao;
    }

    public BigDecimal getBrasileiro() {
        return brasileiro;
    }

    public void setBrasileiro(BigDecimal brasileiro) {
        this.brasileiro = brasileiro;
    }

    public Timestamp getCadastro() {
        return cadastro;
    }

    public void setCadastro(Timestamp cadastro) {
        this.cadastro = cadastro;
    }

    public Timestamp getCarteira() {
        return carteira;
    }

    public void setCarteira(Timestamp carteira) {
        this.carteira = carteira;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Long getClasse() {
        return classe;
    }

    public void setClasse(Long classe) {
        this.classe = classe;
    }

    public Long getCobranca() {
        return cobranca;
    }

    public void setCobranca(Long cobranca) {
        this.cobranca = cobranca;
    }

    public Long getCondicao() {
        return condicao;
    }

    public void setCondicao(Long condicao) {
        this.condicao = condicao;
    }

    public Long getContribuicao() {
        return contribuicao;
    }

    public void setContribuicao(Long contribuicao) {
        this.contribuicao = contribuicao;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getDddcelular() {
        return dddcelular;
    }

    public void setDddcelular(String dddcelular) {
        this.dddcelular = dddcelular;
    }

    public Timestamp getDesativacao() {
        return desativacao;
    }

    public void setDesativacao(Timestamp desativacao) {
        this.desativacao = desativacao;
    }

    public Timestamp getDireitossociais() {
        return direitossociais;
    }

    public void setDireitossociais(Timestamp direitossociais) {
        this.direitossociais = direitossociais;
    }

    public Timestamp getEfetividade() {
        return efetividade;
    }

    public void setEfetividade(Timestamp efetividade) {
        this.efetividade = efetividade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getEstcivil() {
        return estcivil;
    }

    public void setEstcivil(Long estcivil) {
        this.estcivil = estcivil;
    }

    public Timestamp getExame() {
        return exame;
    }

    public void setExame(Timestamp exame) {
        this.exame = exame;
    }

    public Timestamp getFoto() {
        return foto;
    }

    public void setFoto(Timestamp foto) {
        this.foto = foto;
    }

    public GrlPessoa getGrlPessoa() {
        return grlPessoa;
    }

    public void setGrlPessoa(GrlPessoa grlPessoa) {
        this.grlPessoa = grlPessoa;
    }

    public Long getInstrucao() {
        return instrucao;
    }

    public void setInstrucao(Long instrucao) {
        this.instrucao = instrucao;
    }

    public Long getMorto() {
        return morto;
    }

    public void setMorto(Long morto) {
        this.morto = morto;
    }

    public Long getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(Long nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public Timestamp getNascimento() {
        return nascimento;
    }

    public void setNascimento(Timestamp nascimento) {
        this.nascimento = nascimento;
    }

    public Long getNecessidadeEspecial() {
        return necessidadeEspecial;
    }

    public void setNecessidadeEspecial(Long necessidadeEspecial) {
        this.necessidadeEspecial = necessidadeEspecial;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomemae() {
        return nomemae;
    }

    public void setNomemae(String nomemae) {
        this.nomemae = nomemae;
    }

    public String getNomepai() {
        return nomepai;
    }

    public void setNomepai(String nomepai) {
        this.nomepai = nomepai;
    }

    public Long getParentesco() {
        return parentesco;
    }

    public void setParentesco(Long parentesco) {
        this.parentesco = parentesco;
    }

    public Long getProfissao() {
        return profissao;
    }

    public void setProfissao(Long profissao) {
        this.profissao = profissao;
    }

    public Long getProponente1() {
        return proponente1;
    }

    public void setProponente1(Long proponente1) {
        this.proponente1 = proponente1;
    }

    public Long getProponente2() {
        return proponente2;
    }

    public void setProponente2(Long proponente2) {
        this.proponente2 = proponente2;
    }

    public Long getProposta() {
        return proposta;
    }

    public void setProposta(Long proposta) {
        this.proposta = proposta;
    }

    public Timestamp getRecadastro() {
        return recadastro;
    }

    public void setRecadastro(Timestamp recadastro) {
        this.recadastro = recadastro;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public BigDecimal getSexo() {
        return sexo;
    }

    public void setSexo(BigDecimal sexo) {
        this.sexo = sexo;
    }

    public Timestamp getSitdtfim() {
        return sitdtfim;
    }

    public void setSitdtfim(Timestamp sitdtfim) {
        this.sitdtfim = sitdtfim;
    }

    public Timestamp getSitdtini() {
        return sitdtini;
    }

    public void setSitdtini(Timestamp sitdtini) {
        this.sitdtini = sitdtini;
    }

    public Long getTitulo() {
        return titulo;
    }

    public void setTitulo(Long titulo) {
        this.titulo = titulo;
    }

    public Long getTratamento() {
        return tratamento;
    }

    public void setTratamento(Long tratamento) {
        this.tratamento = tratamento;
    }

    public String getUltnome() {
        return ultnome;
    }

    public void setUltnome(String ultnome) {
        this.ultnome = ultnome;
    }

    public Long getVeterano() {
        return veterano;
    }

    public void setVeterano(Long veterano) {
        this.veterano = veterano;
    }

    public BigDecimal getVia() {
        return via;
    }

    public void setVia(BigDecimal via) {
        this.via = via;
    }

    public ScrAssociado getScrAssociado() {
        return scrAssociado;
    }

    public void setScrAssociado(ScrAssociado scrAssociado) {
        this.scrAssociado = scrAssociado;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ScrAssociado{");
        sb.append("id=").append(id);
        sb.append(", admissao=").append(admissao);
        sb.append(", alteracao=").append(alteracao);
        sb.append(", brasileiro=").append(brasileiro);
        sb.append(", cadastro=").append(cadastro);
        sb.append(", carteira=").append(carteira);
        sb.append(", celular='").append(celular).append('\'');
        sb.append(", classe=").append(classe);
        sb.append(", cobranca=").append(cobranca);
        sb.append(", condicao=").append(condicao);
        sb.append(", contribuicao=").append(contribuicao);
        sb.append(", cpf=").append(cpf);
        sb.append(", dddcelular='").append(dddcelular).append('\'');
        sb.append(", desativacao=").append(desativacao);
        sb.append(", direitossociais=").append(direitossociais);
        sb.append(", efetividade=").append(efetividade);
        sb.append(", email='").append(email).append('\'');
        sb.append(", estcivil=").append(estcivil);
        sb.append(", exame=").append(exame);
        sb.append(", foto=").append(foto);
        sb.append(", instrucao=").append(instrucao);
        sb.append(", morto=").append(morto);
        sb.append(", nacionalidade=").append(nacionalidade);
        sb.append(", nascimento=").append(nascimento);
        sb.append(", necessidadeEspecial=").append(necessidadeEspecial);
        sb.append(", nome='").append(nome).append('\'');
        sb.append(", nomemae='").append(nomemae).append('\'');
        sb.append(", nomepai='").append(nomepai).append('\'');
        sb.append(", parentesco=").append(parentesco);
        sb.append(", profissao=").append(profissao);
        sb.append(", proponente1=").append(proponente1);
        sb.append(", proponente2=").append(proponente2);
        sb.append(", proposta=").append(proposta);
        sb.append(", recadastro=").append(recadastro);
        sb.append(", rg='").append(rg).append('\'');
        sb.append(", sexo=").append(sexo);
        sb.append(", sitdtfim=").append(sitdtfim);
        sb.append(", sitdtini=").append(sitdtini);
        sb.append(", titulo=").append(titulo);
        sb.append(", tratamento=").append(tratamento);
        sb.append(", ultnome='").append(ultnome).append('\'');
        sb.append(", veterano=").append(veterano);
        sb.append(", via=").append(via);
        sb.append('}');
        return sb.toString();
    }
}