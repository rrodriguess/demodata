package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class GrlCategoriaVsJdePK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "ID_CATEGORIA", insertable = false, updatable = false, unique = true, nullable = false, precision = 3)
    private long idCategoria;

    @Column(name = "ID_CATEG_JDE", insertable = false, updatable = false, unique = true, nullable = false, length = 3)
    private String idCategJde;

    public GrlCategoriaVsJdePK() {
    }

    public long getIdCategoria() {
        return this.idCategoria;
    }

    public void setIdCategoria(long idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getIdCategJde() {
        return this.idCategJde;
    }

    public void setIdCategJde(String idCategJde) {
        this.idCategJde = idCategJde;
    }
}
