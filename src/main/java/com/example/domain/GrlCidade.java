
package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity(name = "GRL_CIDADE")
@Table(schema = "CAP")
public class GrlCidade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_CIDADE")
    private Long idCidade;

    @Column(name = "IBGE_MUN")
    private Long ibgeMun;

    @Column(name = "NOME_CIDADE")
    private String nomeCidade;

    @Column(name = "UF", columnDefinition = "char(2 byte)")
    private String uf;

    public GrlCidade() {
    }

    public Long getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Long idCidade) {
        this.idCidade = idCidade;
    }

    public Long getIbgeMun() {
        return ibgeMun;
    }

    public void setIbgeMun(Long ibgeMun) {
        this.ibgeMun = ibgeMun;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}