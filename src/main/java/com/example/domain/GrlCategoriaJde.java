package com.example.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity(name = "GRL_CATEGORIA_JDE")
@Table(schema = "CAP")
public class GrlCategoriaJde implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_CATEG_JDE", unique = true, nullable = false)
    private String idCategJde;

    @Column(nullable = false, length = 20)
    private String descricao;

    @OneToMany(mappedBy = "grlCategoriaJde")
    private List<GrlCategoriaVsJde> grlCategoriaVsJdes;

    public GrlCategoriaJde() {
    }

    public String getIdCategJde() {
        return this.idCategJde;
    }

    public void setIdCategJde(String idCategJde) {
        this.idCategJde = idCategJde;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<GrlCategoriaVsJde> getGrlCategoriaVsJdes() {
        return this.grlCategoriaVsJdes;
    }

    public void setGrlCategoriaVsJdes(List<GrlCategoriaVsJde> grlCategoriaVsJdes) {
        this.grlCategoriaVsJdes = grlCategoriaVsJdes;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GrlCategoriaJde{");
        sb.append("idCategJde='").append(idCategJde).append('\'');
        sb.append(", descricao='").append(descricao).append('\'');
        sb.append('}');
        return sb.toString();
    }
}