package com.example.domain;

import com.example.repository.GrlPessoaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class ListOfGrlPessoas {

    @Autowired
    private GrlPessoaDao grlPessoaDao;

    public List<GrlPessoa> getGrlPessoas() {
        List<GrlPessoa> grlPessoas = grlPessoaDao.getListGrlPessoas();

        return grlPessoas;
    }
}
