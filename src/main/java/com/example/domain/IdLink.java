package com.example.domain;

import com.example.repository.GrlPessoaDao;

import java.util.List;

/**
 * Created by user on 09/09/16.
 */
public class IdLink {

    private final GrlPessoaDao grlPessoaDao;

    public IdLink(GrlPessoaDao grlPessoaDao) {
        this.grlPessoaDao = grlPessoaDao;
    }

    public void insert(GrlPessoa pGrlPessoa) {
        DuplicationRecords records = new DuplicationRecords(pGrlPessoa, grlPessoaDao);
        List<GrlPessoa> grlPessoasDuplicadas = records.getListOfDuplicates();

        Long idLink = find(grlPessoasDuplicadas);
        if (idLink != 0l) {
            for (GrlPessoa grlPessoa : grlPessoasDuplicadas) {
                if (grlPessoa.getIdPessoaLink() != null) {
                    grlPessoa.setIdPessoaLink(idLink);
//                grlPessoaDao.save(grlPessoa);
                }
            }
        } else {
            pGrlPessoa.setIdPessoaLink(pGrlPessoa.getIdPessoa());
//            grlPessoaDao.save(pGrlPessoa);
        }
    }

    public Long find(List<GrlPessoa> grlPessoasDuplicadas) {
        Long idLink = 0l;

        for (GrlPessoa pessoa : grlPessoasDuplicadas) {
            if (pessoa.getIdPessoaLink() != null)
                idLink = pessoa.getIdPessoaLink();
        }

        return idLink;
    }
}
