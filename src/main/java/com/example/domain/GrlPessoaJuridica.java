package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity(name = "GRL_PESSOA_JURIDICA")
@Table(schema = "CAP")
public class GrlPessoaJuridica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_PESSOA")
    private Long idPessoa;

    @Column(name = "CAPITAL_SOCIAL")
    private Long capitalSocial;

    @Column(name = "CNPJ")
    private Long cnpj;

    @Column(name = "FANTASIA")
    private String fantasia;

    @Column(name = "INICIO_ATIVIDADES")
    private Timestamp inicioAtividades;

    @Column(name = "INSCRICAO_ESTADUAL")
    private String inscricaoEstadual;

    @Column(name = "INSCRICAO_MUNICIPAL")
    private String inscricaoMunicipal;

    @Column(name = "PESQUISA_FANTASIA")
    private String pesquisaFantasia;

    @Column(name = "RECADASTRAMENTO")
    private Timestamp recadastramento;

    @Column(name = "ID_PORTE")
    private Long idPorte;

    public GrlPessoaJuridica() {
    }

    public Long getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Long getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(Long capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public Long getCnpj() {
        return cnpj;
    }

    public void setCnpj(Long cnpj) {
        this.cnpj = cnpj;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public Timestamp getInicioAtividades() {
        return inicioAtividades;
    }

    public void setInicioAtividades(Timestamp inicioAtividades) {
        this.inicioAtividades = inicioAtividades;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getInscricaoMunicipal() {
        return inscricaoMunicipal;
    }

    public void setInscricaoMunicipal(String inscricaoMunicipal) {
        this.inscricaoMunicipal = inscricaoMunicipal;
    }

    public String getPesquisaFantasia() {
        return pesquisaFantasia;
    }

    public void setPesquisaFantasia(String pesquisaFantasia) {
        this.pesquisaFantasia = pesquisaFantasia;
    }

    public Timestamp getRecadastramento() {
        return recadastramento;
    }

    public void setRecadastramento(Timestamp recadastramento) {
        this.recadastramento = recadastramento;
    }

    public Long getIdPorte() {
        return idPorte;
    }

    public void setIdPorte(Long idPorte) {
        this.idPorte = idPorte;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GrlPessoaJuridica{");
        sb.append("idPessoa=").append(idPessoa);
        sb.append(", capitalSocial=").append(capitalSocial);
        sb.append(", cnpj=").append(cnpj);
        sb.append(", fantasia='").append(fantasia).append('\'');
        sb.append(", inicioAtividades=").append(inicioAtividades);
        sb.append(", inscricaoEstadual='").append(inscricaoEstadual).append('\'');
        sb.append(", inscricaoMunicipal='").append(inscricaoMunicipal).append('\'');
        sb.append(", pesquisaFantasia='").append(pesquisaFantasia).append('\'');
        sb.append(", recadastramento=").append(recadastramento);
        sb.append(", idPorte=").append(idPorte);
        sb.append('}');
        return sb.toString();
    }
}

