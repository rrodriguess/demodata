package com.example.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

@Entity(name = "GRL_PESSOA")
@Table(schema = "CAP")
public class GrlPessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GrlPessoaSequence")
    @SequenceGenerator(name = "GrlPessoaSequence", sequenceName = "CAP.GRL_PESSOA_SEQ", allocationSize = 1)
    @Column(name = "ID_PESSOA")
    private Long idPessoa;

    @Column(name = "ID_PESSOA_LINK")
    private Long idPessoaLink;

    @Column(name = "NOME")
    private String nome;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CATEGORIA")
    private GrlCategoria grlCategoria;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CATEG_JDE")
    private GrlCategoriaJde grlCategoriaJde;

    @Column(name = "NOME_PESQUISA")
    private String nomePesquisa;

    @Column(name = "PESSOA_TP")
    private Long pessoaTp = 0L;

    @Column(name = "CODIGO")
    private Long codigo;

    @Column(name = "DT_CAPTURAIMAGEM")
    private Date dtCapturaImagem;

    @Column(name = "SANEADO")
    private Timestamp saneado;

    @Column(name = "EXCLUIDO")
    private Timestamp excluido;

    @Column(name = "EXPORTADO")
    private Timestamp exportado;

    @Column(name = "ALTERADO")
    private Timestamp alterado;

    @Column(name = "PREFERENCIAL_JDE")
    private Integer preferencialJDE;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PESSOA", insertable = false, updatable = false)
    private GrlPessoaJuridica grlPessoaJuridica;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PESSOA", insertable = false, updatable = false)
    private GrlPessoaFisica grlPessoaFisica;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PESSOA", insertable = false, updatable = false)
    private ScrAssociado scrAssociado;

    public Long getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Long idPessoa) {
        this.idPessoa = idPessoa;
    }

    public Long getIdPessoaLink() {
        return idPessoaLink;
    }

    public void setIdPessoaLink(Long idPessoaLink) {
        this.idPessoaLink = idPessoaLink;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public GrlCategoria getGrlCategoria() {
        return grlCategoria;
    }

    public void setGrlCategoria(GrlCategoria grlCategoria) {
        this.grlCategoria = grlCategoria;
    }

    public GrlCategoriaJde getGrlCategoriaJde() {
        return grlCategoriaJde;
    }

    public void setGrlCategoriaJde(GrlCategoriaJde grlCategoriaJde) {
        this.grlCategoriaJde = grlCategoriaJde;
    }

    public String getNomePesquisa() {
        return nomePesquisa;
    }

    public void setNomePesquisa(String nomePesquisa) {
        this.nomePesquisa = nomePesquisa;
    }

    public Long getPessoaTp() {
        return pessoaTp;
    }

    public void setPessoaTp(Long pessoaTp) {
        this.pessoaTp = pessoaTp;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Date getDtCapturaImagem() {
        return dtCapturaImagem;
    }

    public void setDtCapturaImagem(Date dtCapturaImagem) {
        this.dtCapturaImagem = dtCapturaImagem;
    }

    public Timestamp getSaneado() {
        return saneado;
    }

    public void setSaneado(Timestamp saneado) {
        this.saneado = saneado;
    }

    public Timestamp getExcluido() {
        return excluido;
    }

    public void setExcluido(Timestamp excluido) {
        this.excluido = excluido;
    }

    public Timestamp getExportado() {
        return exportado;
    }

    public void setExportado(Timestamp exportado) {
        this.exportado = exportado;
    }

    public Timestamp getAlterado() {
        return alterado;
    }

    public void setAlterado(Timestamp alterado) {
        this.alterado = alterado;
    }

    public Integer getPreferencialJDE() {
        return preferencialJDE;
    }

    public void setPreferencialJDE(Integer preferencialJDE) {
        this.preferencialJDE = preferencialJDE;
    }

    public GrlPessoaJuridica getGrlPessoaJuridica() {
        return grlPessoaJuridica;
    }

    public void setGrlPessoaJuridica(GrlPessoaJuridica grlPessoaJuridica) {
        this.grlPessoaJuridica = grlPessoaJuridica;
    }

    public GrlPessoaFisica getGrlPessoaFisica() {
        return grlPessoaFisica;
    }

    public void setGrlPessoaFisica(GrlPessoaFisica grlPessoaFisica) {
        this.grlPessoaFisica = grlPessoaFisica;
    }

    public ScrAssociado getScrAssociado() {
        return scrAssociado;
    }

    public void setScrAssociado(ScrAssociado scrAssociado) {
        this.scrAssociado = scrAssociado;
    }
}
