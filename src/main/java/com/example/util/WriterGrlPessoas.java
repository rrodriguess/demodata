package com.example.util;

import com.example.domain.GrlPessoa;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by user on 09/09/16.
 */
public class WriterGrlPessoas {

    public void action(String fileName, List<GrlPessoa> grlPessoas) {
        try {
            FileOutputStream arquivo = new FileOutputStream("pessoas/" + fileName + ".txt");
            PrintWriter printer = new PrintWriter(arquivo);

            for (GrlPessoa grlPessoa : grlPessoas) {
                printer.print("Nome: " + grlPessoa.getNome() +
                        " Id: " + grlPessoa.getIdPessoa() +
                        " IdLink: " + grlPessoa.getIdPessoaLink() +
                        "\n");
            }

            printer.close();
            arquivo.close();

        } catch (FileNotFoundException e ) {
            e.printStackTrace();
            System.out.println("Erro ao escrever o arquivo!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erro ao acessar o arquivo");
        }
    }
}
