package com.example.util;

import com.example.domain.GrlPessoa;

/**
 * Created by user on 09/09/16.
 */
public class DocumentPessoaFisica implements Document{
    @Override
    public Long getDocument(GrlPessoa grlPessoa) {

        if (grlPessoa.getGrlPessoaFisica() != null &&
                grlPessoa.getGrlPessoaFisica().getCpf() != null &&
                grlPessoa.getGrlPessoaFisica().getCpf() != 0) {
            return grlPessoa.getGrlPessoaFisica().getCpf();
        }
        return null;
    }
}
