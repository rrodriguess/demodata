package com.example.util;

import com.example.domain.GrlPessoa;

/**
 * Created by user on 09/09/16.
 */
public interface Document {

    Long getDocument(GrlPessoa grlPessoa);
}
