package com.example.util;

import com.example.domain.GrlPessoa;

/**
 * Created by user on 09/09/16.
 */
public class DocumentAssociado implements Document {
    @Override
    public Long getDocument(GrlPessoa grlPessoa) {

        if (grlPessoa.getScrAssociado() != null &&
                grlPessoa.getScrAssociado().getCpf() != null &&
                grlPessoa.getScrAssociado().getCpf() != 0) {
            return grlPessoa.getScrAssociado().getCpf();
        }
        return null;
    }
}
