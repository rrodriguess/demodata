package com.example.util;

import com.example.domain.GrlPessoa;

/**
 * Created by user on 09/09/16.
 */
public class DocumentPessoaJuridica implements Document {
    @Override
    public Long getDocument(GrlPessoa grlPessoa) {
        if (grlPessoa.getGrlPessoaJuridica() != null &&
                grlPessoa.getGrlPessoaJuridica().getCnpj() != null &&
                grlPessoa.getGrlPessoaJuridica().getCnpj() != 0)
            return grlPessoa.getGrlPessoaJuridica().getCnpj();

        return null;
    }
}
