# Demodata
Projeto desenvolvido com a finalidade de sanear dados exportados que não possuem pessoa_id_link.

## Requisitos:
    1. JDK 8
    2. GIT
    3. Maven 3.3.9 ou superior.
    4. SGBD - Oracle.

## Desenvolvimento:
    Clone o projeto através da uri: git clone git@bitbucket.org:rrodriguess/demodata.git e o importe para sua IDE
    predileta.

## Configuração:
   A aplicação utiliza SPRING BOOT como framework e SPRING DATA para persistência com a base.

## Execução em desenvolvimento:
   Navegue até a classe DemoDataAplication e execute o programa com RUN.
   A aplicação irá gerar uma lista de pessoas que estão com exportado nulo irá gerar um arquivo dentro do diretório
   pessoas chamado antes.txt, onde armazenará a informação.
   Após isso, irá persistir a informação na base, imprimir as alterações no console e gravar em um  outro arquivo
   chamado depois.txt e finalizar o programa.

